<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<sec:authentication var="principal" property="principal" />
<header class="main-header">
	<!-- Logo -->
	<a href="<c:url value="/home" />" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><b>L</b></span> <!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>Loteria</b></span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
			role="button"> <span class="sr-only">Toggle navigation</span> <span
			class="icon-bar"></span> <span class="icon-bar"></span> <span
			class="icon-bar"></span>
		</a>

		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<sec:authorize access="isAnonymous()">
					<li class="dropdown user user-menu"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"> <span
							class="hidden-xs"><c:out
									value="Registrarse / Iniciar sesi�n" /></span>
					</a>
						<ul class="dropdown-menu">
							<li class="user-footer">
								<div class="pull-left">
									<a href='<c:url value="/signup" />'
										class="btn btn-default btn-flat">Registrarse</a>
								</div>
								<div class="pull-right">
									<a href="<c:url value="/login" />"
										class="btn btn-default btn-flat">Iniciar sesi�n</a>
								</div>
							</li>
						</ul></li>
				</sec:authorize>
				<sec:authorize access="isAuthenticated()">
					<li class="dropdown user user-menu"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"> <img
							src="<c:url value="/resources/images/user/adrian.jpg" />"
							class="user-image" alt="User Image"> <span
							class="hidden-xs"><c:out value="${principal.fullname}" /></span>
					</a>
						<ul class="dropdown-menu">
							<!-- User image -->
							<li class="user-header"><img
								src="<c:url value="/resources/images/user/adrian.jpg" />"
								class="img-circle" alt="User Image">

								<p>
									<c:out value="${principal.username}" />
								</p></li>
							<li class="user-footer">
								<div class="pull-left">
									<a href="<c:url value="/user/" />"
										class="btn btn-default btn-flat">Perfil</a>
								</div>
								<div class="pull-right">
									<a href="<c:url value="/login?logout" />"
										class="btn btn-default btn-flat">Logout</a>
								</div>
							</li>
						</ul></li>
				</sec:authorize>
			</ul>
		</div>
	</nav>
</header>