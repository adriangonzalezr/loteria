<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<aside class="main-sidebar">
	<section class="sidebar">
		<ul class="sidebar-menu">
			<li class="header">Men�</li>
			<li class="treeview"><a href="#"> <i class="fa fa-dashboard"></i>
					<span>Juegos</span> <span class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="<c:url value="/gameUser/create" />"><i
							class="fa fa-circle-o"></i>Crear cart�n</a></li>
				</ul></li>
			<li class="treeview"><a href="#"> <i class="fa fa-files-o"></i>
					<span>Reportes</span> <span class="pull-right-container"> <i
						class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
				<ul class="treeview-menu">
					<li><a href="<c:url value="/report/dailyWinners" />"><i
							class="fa fa-circle-o"></i>Ganadores diarios</a></li>
					<li><a href="<c:url value="/report/historicalWinners" />"><i
							class="fa fa-circle-o"></i>Ganadores historicos</a></li>
					<sec:authorize url="/report/funds">
						<li><a href="<c:url value="/report/funds" />"><i
								class="fa fa-circle-o"></i>Montos acumulados</a></li>
					</sec:authorize>
					<sec:authorize url="/report/activityPlayers">
						<li><a href="<c:url value="/report/activityPlayers" />"><i
								class="fa fa-circle-o"></i>Actividad de jugadores</a></li>
					</sec:authorize>
				</ul></li>

		</ul>
	</section>
</aside>