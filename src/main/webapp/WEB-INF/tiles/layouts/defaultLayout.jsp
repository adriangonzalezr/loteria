<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Loteria</title>

<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet"
	href="<c:url value="/resources/dist/css/AdminLTE.min.css" />" />
<link rel="stylesheet"
	href="<c:url value="/resources/dist/css/skins/_all-skins.min.css" />" />

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<tiles:insertAttribute name="header" />

		<tiles:insertAttribute name="menu" />

		<tiles:insertAttribute name="body" />

		<tiles:insertAttribute name="footer" />
	</div>

	<script
		src="<c:url value="/resources/plugins/jQuery/jquery-2.2.3.min.js" />"></script>
	<script
		src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
	<script
		src="<c:url value="/resources/plugins/slimScroll/jquery.slimscroll.min.js" />"></script>
	<script
		src="<c:url value="/resources/plugins/fastclick/fastclick.js" />"></script>
	<script src="<c:url value="/resources/dist/js/app.min.js" />"></script>
</body>
</html>