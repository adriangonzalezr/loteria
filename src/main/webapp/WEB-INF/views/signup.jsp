<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>AdminLTE 2 | Registration Page</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet"
	href="<c:url value="/resources/dist/css/AdminLTE.min.css" />" />
<link rel="stylesheet" href="/resources/plugins/iCheck/square/blue.css">
</head>
<body class="hold-transition register-page">
	<div class="register-box">
		<div class="register-logo">
			<a href="../../index2.html"><b>Loteria</b></a>
		</div>

		<div class="register-box-body">
			<p class="login-box-msg">Registrarse como usuario nuevo</p>


			<c:url var="signupFormAction" value="/signup" />
			<form:form method="post" action="${signupFormAction}"
				modelAttribute="signupForm">

				<div class="form-group has-feedback">
					<input id="user.username" name="user.username" type="email"
						class="form-control" placeholder="Email"> <span
						class="glyphicon glyphicon-envelope form-control-feedback"></span>
					<form:errors path="user.username" />
				</div>
				<div class="form-group has-feedback">
					<input id="user.firstname" name="user.firstname" type="text"
						class="form-control" placeholder="Nombres"> <span
						class="glyphicon glyphicon-user form-control-feedback"></span>
					<form:errors path="user.firstname" />
				</div>
				<div class="form-group has-feedback">
					<input id="user.lastname" name="user.lastname" type="text"
						class="form-control" placeholder="Apellidos"> <span
						class="glyphicon glyphicon-user form-control-feedback"></span>
					<form:errors path="user.lastname" />
				</div>
				<div class="form-group has-feedback">
					<input id="user.password" name="user.password" type="password"
						class="form-control" placeholder="Password"> <span
						class="glyphicon glyphicon-lock form-control-feedback"></span>
					<form:errors path="user.password" />
				</div>
				<div class="row">
					<div class="col-xs-8"></div>
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">Registrar</button>
					</div>
				</div>
			</form:form>

			<a href='<c:url value="/login" />' class="text-center">Ya estoy
				registrado</a>
		</div>
	</div>

	<script src="/resources/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="/resources/bootstrap/js/bootstrap.min.js"></script>
	<script src="/resources/plugins/iCheck/icheck.min.js"></script>
	<script>
		$(function() {
			$('input').iCheck({
				checkboxClass : 'icheckbox_square-blue',
				radioClass : 'iradio_square-blue',
				increaseArea : '20%' // optional
			});
		});
	</script>
</body>
</html>
