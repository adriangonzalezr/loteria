<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Loteria | Login</title>

<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<link rel="stylesheet"
	href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet"
	href="<c:url value="/resources/dist/css/AdminLTE.min.css" />" />
<link rel="stylesheet"
	href="<c:url value="/resources/dist/css/skins/_all-skins.min.css" />" />

</head>
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<b>Lotería</b>
		</div>
		<div class="login-box-body">
			<p class="login-box-msg">Inicia sesión</p>

			<form action="login" method="post">
				<div class="form-group has-feedback">
					<input type="text" class="form-control" placeholder="Username"
						name="username"> <span
						class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" class="form-control" placeholder="Password"
						name="password"> <span
						class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-8">
						<c:if test="${param.error != null}">
							<div class="alert alert-danger">
								<p>Nombre de usuario o contraseña inválida</p>
							</div>
						</c:if>
						<c:if test="${param.logout != null}">
							<div class="alert alert-success">
								<p>Te has desconectado correctamente</p>
							</div>
						</c:if>
					</div>
					<div class="col-xs-4">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<button type="submit" class="btn btn-primary btn-block btn-flat">Sign
							In</button>
					</div>
				</div>
			</form>
		</div>
	</div>

	<script
		src="<c:url value="/resources/plugins/jQuery/jquery-2.2.3.min.js" />"></script>
	<script
		src="<c:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
	<script src="<c:url value="/resources/plugins/iCheck/icheck.min.js" />"></script>
	<script>
		$(function() {
			$('input').iCheck({
				checkboxClass : 'icheckbox_square-blue',
				radioClass : 'iradio_square-blue',
				increaseArea : '20%' // optional
			});
		});
	</script>
</body>
</html>
