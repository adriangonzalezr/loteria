<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Comprar cart�n</h1>
		<ol class="breadcrumb">
			<li><a href="<c:url value="/" />"><i class="fa fa-dashboard"></i>
					Home</a></li>
			<li class="active">Comprar cart�n</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Comprar cart�n</h3>
			</div>
			<div class="box-body">
				<c:url var="gameUserFormAction" value="/gameUser/create" />
				<form:form method="post" action="${gameUserFormAction}"
					modelAttribute="gameUserForm">
					<div class="row">
						<div class="form-group col-xs-2">
							<form:label path="gameUser.number1">N�mero 1: </form:label>
							<form:input path="gameUser.number1" cssClass="form-control" />
							<form:errors path="gameUser.number1" />
						</div>

						<div class="form-group col-xs-2">
							<form:label path="gameUser.number2">N�mero 2: </form:label>
							<form:input path="gameUser.number2" cssClass="form-control" />
							<form:errors path="gameUser.number2" />
						</div>

						<div class="form-group col-xs-2">
							<form:label path="gameUser.number3">N�mero 3: </form:label>
							<form:input path="gameUser.number3" cssClass="form-control" />
							<form:errors path="gameUser.number3" />
						</div>

						<div class="form-group col-xs-2">
							<form:label path="gameUser.number4">N�mero 4: </form:label>
							<form:input path="gameUser.number4" cssClass="form-control" />
							<form:errors path="gameUser.number4" />
						</div>

						<div class="form-group col-xs-2">
							<form:label path="gameUser.number5">N�mero 5: </form:label>
							<form:input path="gameUser.number5" cssClass="form-control" />
							<form:errors path="gameUser.number5" />
						</div>

						<div class="form-group col-xs-2">
							<form:label path="gameUser.number6">N�mero 6: </form:label>
							<form:input path="gameUser.number6" cssClass="form-control" />
							<form:errors path="gameUser.number6" />
						</div>

						<div class="form-group col-xs-2">
							<c:if test="${not empty error}">
								<c:out value="${error}" />
							</c:if>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-xs-3">
							<button type="submit" class="btn btn-primary">Comprar</button>
						</div>
					</div>
				</form:form>
			</div>

			<div class="box-footer"></div>
		</div>
	</section>
</div>
