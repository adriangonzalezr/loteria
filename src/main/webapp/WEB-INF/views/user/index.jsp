<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Mi perfil</h1>
		<ol class="breadcrumb">
			<li><a href="<c:url value="/home" />"><i
					class="fa fa-dashboard"></i> Mi perfil</a></li>
			<li class="active">Mi perfil</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Mi perfil</h3>
			</div>
			<div class="box-body">

				<form:form method="post" modelAttribute="userForm">
					<form:hidden path="user.id" />

					<div class="row">
						<div class="form-group col-xs-3">
							<form:label path="user.funds">Saldo</form:label>
							<form:input path="user.funds" readonly="true" disabled="true"
								cssClass="form-control" />
						</div>

						<div class="form-group col-xs-3">
							<form:label path="charge">Carga</form:label>
							<form:input path="charge" cssClass="form-control" type="number"
								min="0" max="1000000" />
							<form:errors path="charge" />
						</div>
					</div>

					<div class="row">

						<div class="form-group col-xs-3">
							<button type="submit" class="btn btn-primary">Cargar</button>
						</div>
					</div>

				</form:form>


				<c:set var="totalReward" value="${0}" />
				<table class="table table-bordered">
					<tr>
						<th colspan="4">Juegos</th>
					</tr>
					<tr>
						<th>Fecha</th>
						<th>N�mero 1</th>
						<th>N�mero 2</th>
						<th>N�mero 3</th>
						<th>N�mero 4</th>
						<th>N�mero 5</th>
						<th>N�mero 6</th>
						<th>Aciertos</th>
						<th>Premio</th>
					</tr>
					<c:forEach items="${userForm.user.gameUsers}" var="gameUser">
						<tr>
							<td><c:out value="${gameUser.game.gameDate}" /></td>
							<td><c:out value="${gameUser.number1}" /></td>
							<td><c:out value="${gameUser.number2}" /></td>
							<td><c:out value="${gameUser.number3}" /></td>
							<td><c:out value="${gameUser.number4}" /></td>
							<td><c:out value="${gameUser.number5}" /></td>
							<td><c:out value="${gameUser.number6}" /></td>
							<td><c:out value="${gameUser.gameUserReviewed.hits}" /></td>
							<td><c:out value="${gameUser.gameUserReviewed.reward}" /></td>
						</tr>

						<c:set var="totalReward"
							value="${totalReward + gameUser.gameUserReviewed.reward}" />
					</c:forEach>
				</table>
				Premios totales:
				<c:out value="${totalReward}" />
			</div>

			<div class="box-footer"></div>
		</div>
	</section>
</div>
