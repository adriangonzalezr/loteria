<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Actividad de jugadores</h1>
		<ol class="breadcrumb">
			<li><a href="<c:url value="/" />"><i class="fa fa-dashboard"></i>
					Home</a></li>
			<li class="active">Actividad de jugadores</li>
		</ol>
	</section>

	<section class="content">

		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Jugadores Activos</h3>
			</div>
			<div class="box-body">
				<table class="table table-bordered">
					<tr>
						<th colspan="4">Jugadores Activos</th>
					</tr>
					<tr>
						<th>Nombre de usuario</th>
						<th>Nombre</th>
						<th>Apellido</th>
					</tr>
					<c:forEach items="${activeUsers}" var="user">
						<tr>
							<td><c:out value="${user.username}" /></td>
							<td><c:out value="${user.firstname}" /></td>
							<td><c:out value="${user.lastname}" /></td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<div class="box-footer"></div>
			<br />
		</div>



		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Jugadores Inactivos</h3>
			</div>
			<div class="box-body">
				<table class="table table-bordered">
					<tr>
						<th colspan="4">Jugadores Inactivos</th>
					</tr>
					<tr>
						<th>Nombre de usuario</th>
						<th>Nombre</th>
						<th>Apellido</th>
					</tr>
					<c:forEach items="${inActiveUsers}" var="user">
						<tr>
							<td><c:out value="${user.username}" /></td>
							<td><c:out value="${user.firstname}" /></td>
							<td><c:out value="${user.lastname}" /></td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<div class="box-footer"></div>
			<br />
		</div>
	</section>
</div>
