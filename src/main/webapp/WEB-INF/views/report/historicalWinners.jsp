<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Ganadores históricos</h1>
		<ol class="breadcrumb">
			<li><a href="<c:url value="/" />"><i class="fa fa-dashboard"></i>
					Home</a></li>
			<li class="active">Ganadores históricos</li>
		</ol>
	</section>

	<section class="content">

		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Ganadores con 3 aciertos</h3>
			</div>
			<div class="box-body">
				<table class="table table-bordered">
					<tr>
						<th colspan="5">Ganadores con 3 aciertos</th>
					</tr>
					<tr>
						<th>Nombre de usuario</th>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Premio</th>
						<th>Juego</th>
					</tr>
					<c:forEach items="${hits3}" var="gameUserReviewed">
						<tr>
							<td><c:out
									value="${gameUserReviewed.gameUser.user.username}" /></td>
							<td><c:out
									value="${gameUserReviewed.gameUser.user.firstname}" /></td>
							<td><c:out
									value="${gameUserReviewed.gameUser.user.lastname}" /></td>
							<td><c:out value="${gameUserReviewed.reward}" /></td>
							<td><c:out
									value="${gameUserReviewed.gameUser.game.gameDate}" /></td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<div class="box-footer"></div>
			<br />
		</div>

		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Ganadores con 4 aciertos</h3>
			</div>
			<div class="box-body">
				<table class="table table-bordered">
					<tr>
						<th colspan="5">Ganadores con 4 aciertos</th>
					</tr>
					<tr>
						<th>Nombre de usuario</th>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Premio</th>
						<th>Juego</th>
					</tr>
					<c:forEach items="${hits4}" var="gameUserReviewed">
						<tr>
							<td><c:out
									value="${gameUserReviewed.gameUser.user.username}" /></td>
							<td><c:out
									value="${gameUserReviewed.gameUser.user.firstname}" /></td>
							<td><c:out
									value="${gameUserReviewed.gameUser.user.lastname}" /></td>
							<td><c:out value="${gameUserReviewed.reward}" /></td>
							<td><c:out
									value="${gameUserReviewed.gameUser.game.gameDate}" /></td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<div class="box-footer"></div>
			<br />
		</div>

		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Ganadores con 5 aciertos</h3>
			</div>
			<div class="box-body">
				<table class="table table-bordered">
					<tr>
						<th colspan="5">Ganadores con 5 aciertos</th>
					</tr>
					<tr>
						<th>Nombre de usuario</th>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Premio</th>
						<th>Juego</th>
					</tr>
					<c:forEach items="${hits5}" var="gameUserReviewed">
						<tr>
							<td><c:out
									value="${gameUserReviewed.gameUser.user.username}" /></td>
							<td><c:out
									value="${gameUserReviewed.gameUser.user.firstname}" /></td>
							<td><c:out
									value="${gameUserReviewed.gameUser.user.lastname}" /></td>
							<td><c:out value="${gameUserReviewed.reward}" /></td>
							<td><c:out
									value="${gameUserReviewed.gameUser.game.gameDate}" /></td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<div class="box-footer"></div>
			<br />
		</div>

		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Ganadores con 6 aciertos</h3>
			</div>
			<div class="box-body">
				<table class="table table-bordered">
					<tr>
						<th colspan="5">Ganadores con 6 aciertos</th>
					</tr>
					<tr>
						<th>Nombre de usuario</th>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Premio</th>
						<th>Juego</th>
					</tr>
					<c:forEach items="${hits6}" var="gameUserReviewed">
						<tr>
							<td><c:out
									value="${gameUserReviewed.gameUser.user.username}" /></td>
							<td><c:out
									value="${gameUserReviewed.gameUser.user.firstname}" /></td>
							<td><c:out
									value="${gameUserReviewed.gameUser.user.lastname}" /></td>
							<td><c:out value="${gameUserReviewed.reward}" /></td>
							<td><c:out
									value="${gameUserReviewed.gameUser.game.gameDate}" /></td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<div class="box-footer"></div>
			<br />
		</div>
	</section>
</div>
