<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Monto acumulado</h1>
		<ol class="breadcrumb">
			<li><a href="<c:url value="/" />"><i class="fa fa-dashboard"></i>
					Home</a></li>
			<li class="active">Monto acumulado</li>
		</ol>
	</section>

	<section class="content">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Monto acumulado</h3>
			</div>
			<div class="box-body">
				El monto acumulado es de: $
				<c:out value="${company.funds}" />
			</div>
			<div class="box-footer"></div>
			<br />
		</div>
	</section>
</div>
