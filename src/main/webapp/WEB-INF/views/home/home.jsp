<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Sorteos y ganadores</h1>
		<ol class="breadcrumb">
			<li><a href="<c:url value="/" />"><i class="fa fa-dashboard"></i>
					Sorteos y ganadores</a></li>
			<li class="active">Sorteos y ganadores</li>
		</ol>
	</section>

	<section class="content">


		<c:forEach items="${gameFinisheds}" var="gameFinished">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">
						Juego del d�a:
						<c:out value="${gameFinished.game.gameDate}" />
					</h3>
				</div>
				<div class="box-body">
					<table class="table table-bordered">
						<tr>
							<th>N�mero 1</th>
							<th>N�mero 2</th>
							<th>N�mero 3</th>
							<th>N�mero 4</th>
							<th>N�mero 5</th>
							<th>N�mero 6</th>
						</tr>
						<tr>
							<td><c:out value="${gameFinished.number1}" /></td>
							<td><c:out value="${gameFinished.number2}" /></td>
							<td><c:out value="${gameFinished.number3}" /></td>
							<td><c:out value="${gameFinished.number4}" /></td>
							<td><c:out value="${gameFinished.number5}" /></td>
							<td><c:out value="${gameFinished.number6}" /></td>
						</tr>
					</table>
					<br /><br />
					<table class="table table-bordered">
						<tr>
							<th colspan="4">Ganadores</th>
						</tr>
						<tr>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Aciertos</th>
							<th>Premio</th>
						</tr>
						<c:forEach items="${gameFinished.game.gameUsers}" var="gameUser">
							<c:if test="${gameUser.gameUserReviewed.reward > 0}">

								<tr>
									<td><c:out value="${gameUser.user.firstname}" /></td>
									<td><c:out value="${gameUser.user.lastname}" /></td>
									<td><c:out value="${gameUser.gameUserReviewed.hits}" /></td>
									<td><c:out value="${gameUser.gameUserReviewed.reward}" /></td>
								</tr>

							</c:if>
						</c:forEach>
					</table>
				</div>
				<div class="box-footer"></div>
				<br />
			</div>
		</c:forEach>

	</section>
</div>
