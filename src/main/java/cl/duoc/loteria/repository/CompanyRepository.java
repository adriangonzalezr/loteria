package cl.duoc.loteria.repository;

import cl.duoc.loteria.entity.Company;

public interface CompanyRepository {

	public Company get();
	
	public void update(Company company);
	
}
