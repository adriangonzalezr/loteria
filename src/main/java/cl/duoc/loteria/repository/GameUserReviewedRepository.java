package cl.duoc.loteria.repository;

import cl.duoc.loteria.entity.GameUser;
import cl.duoc.loteria.entity.GameUserReviewed;

import java.util.List;

public interface GameUserReviewedRepository {

	public List<GameUserReviewed> list();

	public void create(GameUserReviewed gameUserReviewed);

	public List<GameUserReviewed> findByHits(int hits);

}
