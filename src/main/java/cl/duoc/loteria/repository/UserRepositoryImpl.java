package cl.duoc.loteria.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import cl.duoc.loteria.entity.User;

@Repository("userRepository")
public class UserRepositoryImpl implements UserRepository {

	private static final Logger logger = Logger.getLogger(UserRepositoryImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<User> list() {
		List<User> users = this.entityManager.createNamedQuery("User.findAll", User.class).getResultList();

		logger.info(users);

		return users;
	}

	@Override
	public User findByUsername(String username) {
		User user = this.entityManager.createNamedQuery("User.findByUsername", User.class)
				.setParameter("username", username).getSingleResult();

		return user;
	}

	@Override
	public User findById(int id) {
		User user = this.entityManager.createNamedQuery("User.findById", User.class).setParameter("id", id)
				.getSingleResult();

		return user;
	}

	@Override
	public void create(User user) {
		this.entityManager.persist(user);
	}

	@Override
	public void update(User user) {
		this.entityManager.merge(user);
	}

}
