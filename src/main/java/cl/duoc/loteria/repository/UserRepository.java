package cl.duoc.loteria.repository;

import java.util.List;

import cl.duoc.loteria.entity.User;

public interface UserRepository {

	public List<User> list();
	
	public User findByUsername(String username);
	
	public User findById(int id);
	
	public void create(User user);
	
	public void update(User user);

}
