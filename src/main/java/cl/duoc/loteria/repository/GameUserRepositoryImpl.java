package cl.duoc.loteria.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.duoc.loteria.entity.GameUser;

@Repository("gameUserRepository")
public class GameUserRepositoryImpl implements GameUserRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<GameUser> list() {
		List<GameUser> gameUsers = this.entityManager.createNamedQuery("GameUser.findAll", GameUser.class)
				.getResultList();
		return gameUsers;
	}

	@Override
	public List<GameUser> findByUserId(int userId) {
		List<GameUser> gameUsers = this.entityManager.createNamedQuery("GameUser.findByUserId", GameUser.class)
				.setParameter("userId", userId).getResultList();
		return gameUsers;
	}

	@Override
	public void create(GameUser gameUser) {
		this.entityManager.persist(gameUser);
	}

	@Override
	public void delete(GameUser gameUser) {
		this.entityManager.remove(gameUser);
	}

}
