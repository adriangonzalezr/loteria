package cl.duoc.loteria.repository;

import java.util.List;

import cl.duoc.loteria.entity.GameUser;

public interface GameUserRepository {

	public List<GameUser> list();

	public List<GameUser> findByUserId(int userId);

	public void create(GameUser gameUser);
	
	public void delete(GameUser gameUser);
}
