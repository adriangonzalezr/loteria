package cl.duoc.loteria.repository;

import java.util.Date;
import java.util.List;

import cl.duoc.loteria.entity.Game;

public interface GameRepository {

	public List<Game> list();
	
	public Game findById(int id);
	
	public Game findByGameDate(Date gameDate);
	
	public void create(Game game);
}
