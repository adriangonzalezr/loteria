package cl.duoc.loteria.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import cl.duoc.loteria.entity.GameFinished;

@Repository("gameFinishedRepository")
public class GameFinishedRepositoryImpl implements GameFinishedRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<GameFinished> list() {
		List<GameFinished> gamesFinished = this.entityManager
				.createNamedQuery("GameFinished.findAll", GameFinished.class).getResultList();
		return gamesFinished;
	}

	@Override
	public void create(GameFinished gameFinished) {
		this.entityManager.persist(gameFinished);
	}

}
