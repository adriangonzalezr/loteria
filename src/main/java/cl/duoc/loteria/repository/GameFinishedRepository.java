package cl.duoc.loteria.repository;

import java.util.List;

import cl.duoc.loteria.entity.GameFinished;

public interface GameFinishedRepository {

	public List<GameFinished> list();

	public void create(GameFinished gameFinished);

}
