package cl.duoc.loteria.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import cl.duoc.loteria.entity.Game;

@Repository("gameRepository")
public class GameRepositoryImpl implements GameRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Game> list() {
		List<Game> games = this.entityManager.createNamedQuery("Game.findAll", Game.class).getResultList();
		return games;
	}

	@Override
	public Game findById(int id) {
		Game game = this.entityManager.createNamedQuery("Game.findById", Game.class).setParameter("id", id)
				.getSingleResult();
		return game;
	}

	@Override
	public Game findByGameDate(Date gameDate) {
		Game game = this.entityManager.createNamedQuery("Game.findByGameDate", Game.class)
				.setParameter("gameDate", gameDate).getSingleResult();
		return game;
	}

	@Override
	public void create(Game game) {
		this.entityManager.persist(game);
	}

}
