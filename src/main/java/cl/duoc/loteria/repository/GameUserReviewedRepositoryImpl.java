package cl.duoc.loteria.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import cl.duoc.loteria.entity.GameUser;
import cl.duoc.loteria.entity.GameUserReviewed;

@Repository("gameUserReviewedRepository")
public class GameUserReviewedRepositoryImpl implements GameUserReviewedRepository {

	private static final Logger LOGGER = Logger.getLogger(GameUserReviewedRepositoryImpl.class);

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<GameUserReviewed> list() {
		List<GameUserReviewed> gameUserRevieweds = this.entityManager
				.createNamedQuery("GameUserReviewed.findAll", GameUserReviewed.class).getResultList();
		return gameUserRevieweds;
	}

	@Override
	public void create(GameUserReviewed gameUserReviewed) {
		this.entityManager.persist(gameUserReviewed);
	}

	@Override
	public List<GameUserReviewed> findByHits(int hits) {
		List<GameUserReviewed> gameUserRevieweds = new ArrayList<>();
		try {
			gameUserRevieweds = this.entityManager.createNamedQuery("GameUserReviewed.findByHits", GameUserReviewed.class)
					.setParameter("hits", hits).getResultList();
		} catch (Exception e) {
			LOGGER.error(e);
		}

		return gameUserRevieweds;
	}

}
