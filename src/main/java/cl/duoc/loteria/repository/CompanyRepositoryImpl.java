package cl.duoc.loteria.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import cl.duoc.loteria.entity.Company;

@Repository("companyRepository")
public class CompanyRepositoryImpl implements CompanyRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Company get() {
		Company company = this.entityManager.createNamedQuery("Company.findById", Company.class).setParameter("id", 1)
				.getSingleResult();

		return company;
	}

	@Override
	public void update(Company company) {
		this.entityManager.merge(company);
	}

}
