package cl.duoc.loteria.controller;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.duoc.loteria.dto.GeneratedGame;
import cl.duoc.loteria.entity.GameUser;
import cl.duoc.loteria.exception.ServiceException;
import cl.duoc.loteria.model.GameUserForm;
import cl.duoc.loteria.security.CustomUser;
import cl.duoc.loteria.service.GameService;
import cl.duoc.loteria.service.GameUserService;
import cl.duoc.loteria.service.UserService;
import cl.duoc.loteria.util.GenerateGame;

@Controller
@RequestMapping(path = "/gameUser")
public class GameUserController {

	@Autowired
	private GameUserService gameUserService;

	@Autowired
	private UserService userService;

	@Autowired
	private GameService gameService;

	@RequestMapping(path = "/create", method = RequestMethod.GET)
	public String createView(GameUserForm gameUserForm) {
		GeneratedGame generatedGame = GenerateGame.generateGame();

		gameUserForm.getGameUser().setNumber1(generatedGame.getNumber1());
		gameUserForm.getGameUser().setNumber2(generatedGame.getNumber2());
		gameUserForm.getGameUser().setNumber3(generatedGame.getNumber3());
		gameUserForm.getGameUser().setNumber4(generatedGame.getNumber4());
		gameUserForm.getGameUser().setNumber5(generatedGame.getNumber5());
		gameUserForm.getGameUser().setNumber6(generatedGame.getNumber6());

		return "gameUser.create";
	}

	@RequestMapping(path = "/create", method = RequestMethod.POST)
	public String create(@Valid GameUserForm gameUserForm, BindingResult bindingResult, Model model,
			@AuthenticationPrincipal CustomUser customUser) {

		if (bindingResult.hasErrors()) {
			return "gameUser.create";
		}

		GameUser gameUser = gameUserForm.getGameUser();

		try {
			gameUser.setUser(userService.findById(customUser.getId()));
			gameUser.setGame(this.gameService.findByGameDateOrCreate(new Date()));
			this.gameUserService.create(gameUser);

			return "redirect:/user/";
		} catch (ServiceException serviceException) {
			model.addAttribute("error", serviceException.getMessage());
		}

		return "gameUser.create";
	}
}
