package cl.duoc.loteria.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.duoc.loteria.entity.User;
import cl.duoc.loteria.exception.ServiceException;
import cl.duoc.loteria.model.SignupForm;
import cl.duoc.loteria.service.UserService;

@Controller
public class AuthenticationController {

	@Autowired
	private UserService userService;

	@RequestMapping(path = "/login", method = RequestMethod.GET)
	public void login() {
	}

	@RequestMapping(path = "/signup", method = RequestMethod.GET)
	public String signupView(SignupForm signupForm) {

		return "signup";
	}

	@RequestMapping(path = "/signup", method = RequestMethod.POST)
	public String signup(@Valid SignupForm signupForm, BindingResult bindingResult, Model model) {

		if (bindingResult.hasErrors()) {
			return "signup";
		}

		User user = signupForm.getUser();
		try {
			this.userService.create(user);
		} catch (ServiceException serviceException) {
			model.addAttribute("error", serviceException.getMessage());
			return "signup";
		}

		return "login";
	}

}
