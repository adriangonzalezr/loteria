package cl.duoc.loteria.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.duoc.loteria.entity.GameFinished;
import cl.duoc.loteria.exception.ServiceException;
import cl.duoc.loteria.service.GameFinishedService;
import cl.duoc.loteria.service.GameReviewerService;

@Controller
public class HomeController {

	private static final Logger LOGGER = Logger.getLogger(HomeController.class);

	@Autowired
	private GameFinishedService gameFinishedService;

	@Autowired
	private GameReviewerService gameReviewerService;

	@RequestMapping(path = { "/", "/home" }, method = RequestMethod.GET)
	public String home(Model model) {
		try {
			this.gameReviewerService.reviewGames();
		} catch (ServiceException e) {
			LOGGER.error(e);
		}

		List<GameFinished> gameFinisheds = this.gameFinishedService.list();

		model.addAttribute("gameFinisheds", gameFinisheds);

		return "home";
	}

}
