package cl.duoc.loteria.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.duoc.loteria.entity.Company;
import cl.duoc.loteria.entity.GameUserReviewed;
import cl.duoc.loteria.entity.User;
import cl.duoc.loteria.service.CompanyService;
import cl.duoc.loteria.service.GameUserReviewedService;
import cl.duoc.loteria.service.UserService;

@Controller
@RequestMapping("/report")
public class ReportController {

	@Autowired
	private UserService userService;

	@Autowired
	private CompanyService companyService;

	@Autowired
	private GameUserReviewedService gameUserReviewedService;

	@RequestMapping(path = "/dailyWinners", method = RequestMethod.GET)
	public String dailyWinners() {
		return "redirect:/";
	}

	@RequestMapping(path = "/historicalWinners", method = RequestMethod.GET)
	public String historicalWinners(Model model) {
		List<GameUserReviewed> hits3 = this.gameUserReviewedService.findByHits(3);
		List<GameUserReviewed> hits4 = this.gameUserReviewedService.findByHits(4);
		List<GameUserReviewed> hits5 = this.gameUserReviewedService.findByHits(5);
		List<GameUserReviewed> hits6 = this.gameUserReviewedService.findByHits(6);

		model.addAttribute("hits3", hits3);
		model.addAttribute("hits4", hits4);
		model.addAttribute("hits5", hits5);
		model.addAttribute("hits6", hits6);
		
		return "report.historicalWinners";
	}

	@RequestMapping(path = "/funds", method = RequestMethod.GET)
	public String funds(Model model) {
		Company company = this.companyService.get();

		model.addAttribute("company", company);
		return "report.funds";
	}

	@RequestMapping(path = "/activityPlayers", method = RequestMethod.GET)
	public String activityPlayers(Model model) {
		List<User> users = this.userService.list();

		List<User> activeUsers = new ArrayList<>();
		List<User> inActiveUsers = new ArrayList<>();

		for (User user : users) {
			if (user.getGameUsers() == null || user.getGameUsers().isEmpty()) {
				inActiveUsers.add(user);
			} else {
				activeUsers.add(user);
			}
		}

		model.addAttribute("activeUsers", activeUsers);
		model.addAttribute("inActiveUsers", inActiveUsers);

		return "report.activityPlayers";
	}
}
