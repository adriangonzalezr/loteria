package cl.duoc.loteria.controller;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.duoc.loteria.entity.User;
import cl.duoc.loteria.exception.ServiceException;
import cl.duoc.loteria.model.UserForm;
import cl.duoc.loteria.security.CustomUser;
import cl.duoc.loteria.service.GameReviewerService;
import cl.duoc.loteria.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {

	private static final Logger LOGGER = Logger.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@Autowired
	private GameReviewerService gameReviewerService;

	@RequestMapping(path = "/", method = RequestMethod.GET)
	public String indexView(UserForm userForm, Model model, @AuthenticationPrincipal CustomUser customUser) {
		try {
			this.gameReviewerService.reviewGames();
		} catch (ServiceException e) {
			LOGGER.error(e);
		}

		User user = this.userService.findById(customUser.getId());
		userForm.setUser(user);

		return "user.index";
	}

	@RequestMapping(path = "/", method = RequestMethod.POST)
	public String index(@Valid UserForm userForm, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return "user.index";
		}

		User user = this.userService.findById(userForm.getUser().getId());
		user.setFunds(user.getFunds() + userForm.getCharge());

		try {
			this.userService.update(user);
		} catch (ServiceException serviceException) {
			model.addAttribute("error", serviceException.getMessage());
		}

		userForm.setUser(user);

		return "user.index";
	}

}
