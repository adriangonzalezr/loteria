package cl.duoc.loteria.model;

import javax.validation.Valid;

import cl.duoc.loteria.entity.GameUser;

public class GameUserForm {

	@Valid
	private GameUser gameUser;

	public GameUserForm() {
		this.gameUser = new GameUser();
	}

	public GameUser getGameUser() {
		return gameUser;
	}

	public void setGameUser(GameUser gameUser) {
		this.gameUser = gameUser;
	}

}
