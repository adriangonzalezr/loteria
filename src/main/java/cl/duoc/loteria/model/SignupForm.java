package cl.duoc.loteria.model;

import javax.validation.Valid;

import cl.duoc.loteria.entity.User;

public class SignupForm {

	@Valid
	private User user;

	public SignupForm() {
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
