package cl.duoc.loteria.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import cl.duoc.loteria.entity.User;

public class UserForm {

	private User user;

	@Min(value = 0)
	@Max(value = 1000000)
	private int charge;

	public UserForm() {
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getCharge() {
		return charge;
	}

	public void setCharge(int charge) {
		this.charge = charge;
	}

}
