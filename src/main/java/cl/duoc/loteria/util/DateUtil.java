package cl.duoc.loteria.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	public static Date stringToDate(String string) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		return simpleDateFormat.parse(string);
	}
	
	public static boolean hourInRange(Date date) {
		Date startDate = getDate(21, 0, 0);
		Date endDate = getDate(23, 59, 59);
		
		boolean isValid = false;
		if(date.getTime() >= startDate.getTime() && date.getTime() <= endDate.getTime()) {
			isValid = true;
		}
		
		return isValid;
	}
	
	public static Date getDate(int hour, int minute, int second) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.HOUR, hour);
		calendar.set(Calendar.MINUTE, minute);
		calendar.set(Calendar.SECOND, second);
		
		return calendar.getTime();
	}
	
	public static boolean compareDay(Date date1, Date date2) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.format(date1).equals(sdf.format(date2));
	}
}
