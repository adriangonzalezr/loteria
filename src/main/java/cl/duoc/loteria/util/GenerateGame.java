package cl.duoc.loteria.util;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import cl.duoc.loteria.dto.GeneratedGame;

public class GenerateGame {

	public static GeneratedGame generateGame() {
		List<Integer> numbers = IntStream.rangeClosed(1, 98).boxed().collect(Collectors.toList());
		Collections.shuffle(numbers);

		GeneratedGame generatedGame = new GeneratedGame();
		generatedGame.setNumber1(numbers.get(1));
		generatedGame.setNumber2(numbers.get(2));
		generatedGame.setNumber3(numbers.get(3));
		generatedGame.setNumber4(numbers.get(4));
		generatedGame.setNumber5(numbers.get(5));
		generatedGame.setNumber6(numbers.get(6));
		
		return generatedGame;
	}

}
