package cl.duoc.loteria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.duoc.loteria.entity.Company;
import cl.duoc.loteria.entity.GameUser;
import cl.duoc.loteria.entity.User;
import cl.duoc.loteria.exception.ServiceException;
import cl.duoc.loteria.repository.CompanyRepository;
import cl.duoc.loteria.repository.GameUserRepository;
import cl.duoc.loteria.repository.UserRepository;

@Service("gameUserService")
public class GameUserServiceImpl implements GameUserService {

	@Autowired
	private GameUserRepository gameUserRepository;

	@Autowired
	private CompanyRepository companyRepository;

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<GameUser> findByUserId(int userId) {
		List<GameUser> gameUsers = this.gameUserRepository.findByUserId(userId);

		return gameUsers;
	}

	@Override
	@Transactional
	public void create(GameUser gameUser) throws ServiceException {
		User user = gameUser.getUser();
		if(user.getFunds() < 1200) {
			throw new ServiceException("No tiene saldo suficiente");
		}
		
		Company company = this.companyRepository.get();
		company.setFunds(company.getFunds() + 1200);
		this.companyRepository.update(company);
		
		
		user.setFunds(user.getFunds() - 1200);
		this.userRepository.update(user);
		
		gameUser.setPrice(1200);
		this.gameUserRepository.create(gameUser);
	}

}
