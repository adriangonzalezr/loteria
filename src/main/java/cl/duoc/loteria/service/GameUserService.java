package cl.duoc.loteria.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import cl.duoc.loteria.entity.GameUser;
import cl.duoc.loteria.exception.ServiceException;

public interface GameUserService {

	public List<GameUser> findByUserId(int userId);

	public void create(GameUser gameUser) throws ServiceException;
}
