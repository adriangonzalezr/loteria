package cl.duoc.loteria.service;

import java.util.List;

import cl.duoc.loteria.entity.User;
import cl.duoc.loteria.exception.ServiceException;

public interface UserService {

	public List<User> list();

	public User findByUsername(String username);

	public User findById(int id);
	
	public void create(User user) throws ServiceException;
	
	public void update(User user) throws ServiceException;
}
