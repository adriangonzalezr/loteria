package cl.duoc.loteria.service;

import java.util.Date;
import java.util.List;

import cl.duoc.loteria.entity.Game;
import cl.duoc.loteria.exception.ServiceException;

public interface GameService {

	public List<Game> list();
	
	public Game findById(int id);
	
	public Game findByGameDateOrCreate(Date gameDate) throws ServiceException;
	
}
