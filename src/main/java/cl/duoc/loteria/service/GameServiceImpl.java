package cl.duoc.loteria.service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.duoc.loteria.entity.Company;
import cl.duoc.loteria.entity.Game;
import cl.duoc.loteria.exception.ServiceException;
import cl.duoc.loteria.repository.GameRepository;
import cl.duoc.loteria.util.DateUtil;

@Service("gameService")
public class GameServiceImpl implements GameService {
	
	@Autowired
	private GameRepository gameRepository;

	@Override
	public List<Game> list() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional
	public Game findByGameDateOrCreate(Date gameDate) throws ServiceException {

		if(DateUtil.hourInRange(gameDate)) {
			throw new ServiceException("No se puede jugar desde 21:00 y 00:00");
		}
		
		Game game = new Game();
		try {
			game = this.gameRepository.findByGameDate(gameDate);
		}
		catch(EmptyResultDataAccessException emptyResultDataAccessException) {
			game.setCompany(new Company(1));
			game.setGameDate(gameDate);
			this.gameRepository.create(game);
		}
		
		return game;
	}

}
