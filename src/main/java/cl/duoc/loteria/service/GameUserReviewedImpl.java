package cl.duoc.loteria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.duoc.loteria.entity.GameUserReviewed;
import cl.duoc.loteria.repository.GameUserReviewedRepository;

@Service("GameUserReviewedService")
public class GameUserReviewedImpl implements GameUserReviewedService {

	@Autowired
	private GameUserReviewedRepository gameUserReviewedRepository;
	
	@Override
	public List<GameUserReviewed> findByHits(int hits) {
		return this.gameUserReviewedRepository.findByHits(hits);
	}

}
