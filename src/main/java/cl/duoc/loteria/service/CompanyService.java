package cl.duoc.loteria.service;

import cl.duoc.loteria.entity.Company;

public interface CompanyService {

	public Company get();
	
}
