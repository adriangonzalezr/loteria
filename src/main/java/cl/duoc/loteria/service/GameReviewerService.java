package cl.duoc.loteria.service;

import cl.duoc.loteria.exception.ServiceException;

public interface GameReviewerService {

	public void reviewGames() throws ServiceException;
	
}
