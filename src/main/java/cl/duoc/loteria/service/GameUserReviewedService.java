package cl.duoc.loteria.service;

import java.util.List;

import cl.duoc.loteria.entity.GameUserReviewed;

public interface GameUserReviewedService {

	public List<GameUserReviewed> findByHits(int hits);
	
}
