package cl.duoc.loteria.service;

import java.util.List;

import cl.duoc.loteria.entity.GameFinished;

public interface GameFinishedService {

	public List<GameFinished> list();
	
}
