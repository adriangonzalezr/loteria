package cl.duoc.loteria.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.duoc.loteria.entity.Company;
import cl.duoc.loteria.repository.CompanyRepository;

@Service("companyService")
public class CompanyImpl implements CompanyService {

	@Autowired
	private CompanyRepository companyRepository;

	@Override
	public Company get() {
		return this.companyRepository.get();
	}

}
