package cl.duoc.loteria.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.duoc.loteria.dto.GeneratedGame;
import cl.duoc.loteria.entity.Company;
import cl.duoc.loteria.entity.Game;
import cl.duoc.loteria.entity.GameFinished;
import cl.duoc.loteria.entity.GameUser;
import cl.duoc.loteria.entity.GameUserReviewed;
import cl.duoc.loteria.entity.User;
import cl.duoc.loteria.exception.ServiceException;
import cl.duoc.loteria.repository.CompanyRepository;
import cl.duoc.loteria.repository.GameFinishedRepository;
import cl.duoc.loteria.repository.GameRepository;
import cl.duoc.loteria.repository.GameUserReviewedRepository;
import cl.duoc.loteria.repository.UserRepository;
import cl.duoc.loteria.util.DateUtil;
import cl.duoc.loteria.util.GenerateGame;

@Service("gameReviewerService")
public class GameReviewerImpl implements GameReviewerService {

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private GameFinishedRepository gameFinishedRepository;

	@Autowired
	private GameUserReviewedRepository gameUserReviewedRepository;

	@Autowired
	private CompanyRepository companyRepository;

	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional
	public void reviewGames() throws ServiceException {
		List<Game> games = this.gameRepository.list();
		Company company = this.companyRepository.get();

		for (Game game : games) {
			if (game.getGameFinished() == null && !this.ignoreReview(game.getGameDate())) {
				GameFinished gameFinished = new GameFinished();

				GeneratedGame generatedGame = GenerateGame.generateGame();

				gameFinished.setGame(game);
				gameFinished.setNumber1(generatedGame.getNumber1());
				gameFinished.setNumber2(generatedGame.getNumber2());
				gameFinished.setNumber3(generatedGame.getNumber3());
				gameFinished.setNumber4(generatedGame.getNumber4());
				gameFinished.setNumber5(generatedGame.getNumber5());
				gameFinished.setNumber6(generatedGame.getNumber6());

				this.gameFinishedRepository.create(gameFinished);

				for (GameUser gameUser : game.getGameUsers()) {
					if (gameUser.getGameUserReviewed() == null) {
						int hits = 0;
						if (gameFinished.getNumber1() == gameUser.getNumber1()) {
							hits++;
						}
						if (gameFinished.getNumber2() == gameUser.getNumber2()) {
							hits++;
						}
						if (gameFinished.getNumber3() == gameUser.getNumber3()) {
							hits++;
						}
						if (gameFinished.getNumber4() == gameUser.getNumber4()) {
							hits++;
						}
						if (gameFinished.getNumber5() == gameUser.getNumber5()) {
							hits++;
						}
						if (gameFinished.getNumber6() == gameUser.getNumber6()) {
							hits++;
						}

						int reward = 0;
						switch (hits) {
						case 3:
							reward = gameUser.getPrice();
							break;
						case 4:
							reward = gameUser.getPrice() * 15;
							break;
						case 5:
							reward = gameUser.getPrice() * 100;
							break;
						case 6:
							reward = (int) Math.round(company.getFunds() * 0.3);
							break;
						}

						GameUserReviewed gameUserReviewed = new GameUserReviewed();
						gameUserReviewed.setGameUser(gameUser);
						gameUserReviewed.setHits(hits);
						gameUserReviewed.setReward(reward);

						this.gameUserReviewedRepository.create(gameUserReviewed);

						if (reward != 0) {
							company.setFunds(company.getFunds() - reward);
							this.companyRepository.update(company);

							User user = gameUser.getUser();
							user.setFunds(user.getFunds() + reward);
							this.userRepository.update(user);
						}
					}
				}
			}
		}
	}

	private boolean ignoreReview(Date gameDate) {
		boolean ignored = false;

		if (DateUtil.compareDay(gameDate, new Date()) && !DateUtil.hourInRange(gameDate)) {
			ignored = true;
		}

		return ignored;
	}

}
