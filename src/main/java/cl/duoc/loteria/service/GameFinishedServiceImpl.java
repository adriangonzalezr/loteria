package cl.duoc.loteria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.duoc.loteria.entity.GameFinished;
import cl.duoc.loteria.repository.GameFinishedRepository;

@Service("gameFinishedService")
public class GameFinishedServiceImpl implements GameFinishedService {

	@Autowired
	private GameFinishedRepository gameFinishedRepository;

	@Override
	public List<GameFinished> list() {
		List<GameFinished> gameFinisheds = this.gameFinishedRepository.list();
		
		return gameFinisheds;
	}

}
