package cl.duoc.loteria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.duoc.loteria.entity.User;
import cl.duoc.loteria.exception.ServiceException;
import cl.duoc.loteria.repository.UserRepository;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<User> list() {
		return this.userRepository.list();
	}

	@Override
	public User findByUsername(String username) {
		return this.userRepository.findByUsername(username);
	}

	@Override
	public User findById(int id) {
		return this.userRepository.findById(id);
	}

	@Override
	@Transactional
	public void create(User user) throws ServiceException {
		user.setStatus(1);
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		user.setPassword(encoder.encode(user.getPassword()));
		this.userRepository.create(user);
	}
	
	@Override
	@Transactional
	public void update(User user) throws ServiceException {
		this.userRepository.update(user);
	}

}
