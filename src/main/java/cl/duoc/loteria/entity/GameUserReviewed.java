/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.loteria.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Waroz
 */
@Entity
@Table(name = "game_user_reviewed", catalog = "loteria", schema = "")
@NamedQueries({ @NamedQuery(name = "GameUserReviewed.findAll", query = "SELECT g FROM GameUserReviewed g"),
		@NamedQuery(name = "GameUserReviewed.findById", query = "SELECT g FROM GameUserReviewed g WHERE g.id = :id"),
		@NamedQuery(name = "GameUserReviewed.findByHits", query = "SELECT g FROM GameUserReviewed g WHERE g.hits = :hits"),
		@NamedQuery(name = "GameUserReviewed.findByReward", query = "SELECT g FROM GameUserReviewed g WHERE g.reward = :reward"),
		@NamedQuery(name = "GameUserReviewed.findByStatus", query = "SELECT g FROM GameUserReviewed g WHERE g.status = :status") })
public class GameUserReviewed implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Integer id;
	@Basic(optional = false)
	@NotNull
	@Column(name = "hits")
	private int hits;
	@Basic(optional = false)
	@NotNull
	@Column(name = "reward")
	private int reward;
	@Basic(optional = false)
	@NotNull
	@Column(name = "status")
	private int status;
	@JoinColumn(name = "game_user_id", referencedColumnName = "id")
	@OneToOne(optional = false)
	private GameUser gameUser;

	public GameUserReviewed() {
	}

	public GameUserReviewed(Integer id) {
		this.id = id;
	}

	public GameUserReviewed(Integer id, int hits, int reward, int status) {
		this.id = id;
		this.hits = hits;
		this.reward = reward;
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getHits() {
		return hits;
	}

	public void setHits(int hits) {
		this.hits = hits;
	}

	public int getReward() {
		return reward;
	}

	public void setReward(int reward) {
		this.reward = reward;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public GameUser getGameUser() {
		return gameUser;
	}

	public void setGameUser(GameUser gameUser) {
		this.gameUser = gameUser;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof GameUserReviewed)) {
			return false;
		}
		GameUserReviewed other = (GameUserReviewed) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Test.GameUserReviewed[ id=" + id + " ]";
	}

}
