/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.loteria.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Waroz
 */
@Entity
@Table(name = "game", catalog = "loteria", schema = "")
@NamedQueries({ @NamedQuery(name = "Game.findAll", query = "SELECT g FROM Game g"),
		@NamedQuery(name = "Game.findById", query = "SELECT g FROM Game g WHERE g.id = :id"),
		@NamedQuery(name = "Game.findByGameDate", query = "SELECT g FROM Game g WHERE g.gameDate = :gameDate") })
public class Game implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Integer id;
	@Basic(optional = false)
	@NotNull
	@Column(name = "game_date")
	@Temporal(TemporalType.DATE)
	private Date gameDate;
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "game")
	private GameFinished gameFinished;
	@JoinColumn(name = "company_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Company company;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "game")
	private List<GameUser> gameUsers;

	public Game() {
	}

	public Game(Integer id) {
		this.id = id;
	}

	public Game(Integer id, Date gameDate) {
		this.id = id;
		this.gameDate = gameDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getGameDate() {
		return gameDate;
	}

	public void setGameDate(Date gameDate) {
		this.gameDate = gameDate;
	}

	public GameFinished getGameFinished() {
		return gameFinished;
	}

	public void setGameFinished(GameFinished gameFinished) {
		this.gameFinished = gameFinished;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public List<GameUser> getGameUsers() {
		return gameUsers;
	}

	public void setGameUsers(List<GameUser> gameUsers) {
		this.gameUsers = gameUsers;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Game)) {
			return false;
		}
		Game other = (Game) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Test.Game[ id=" + id + " ]";
	}

}
