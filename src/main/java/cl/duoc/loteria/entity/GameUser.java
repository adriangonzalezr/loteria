/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.duoc.loteria.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Waroz
 */
@Entity
@Table(name = "game_user", catalog = "loteria", schema = "")
@NamedQueries({ @NamedQuery(name = "GameUser.findAll", query = "SELECT g FROM GameUser g"),
		@NamedQuery(name = "GameUser.findById", query = "SELECT g FROM GameUser g WHERE g.id = :id"),
		@NamedQuery(name = "GameUser.findByUserId", query = "SELECT g FROM GameUser g WHERE g.user.id = :userId"),
		@NamedQuery(name = "GameUser.findByNumber1", query = "SELECT g FROM GameUser g WHERE g.number1 = :number1"),
		@NamedQuery(name = "GameUser.findByNumber2", query = "SELECT g FROM GameUser g WHERE g.number2 = :number2"),
		@NamedQuery(name = "GameUser.findByNumber3", query = "SELECT g FROM GameUser g WHERE g.number3 = :number3"),
		@NamedQuery(name = "GameUser.findByNumber4", query = "SELECT g FROM GameUser g WHERE g.number4 = :number4"),
		@NamedQuery(name = "GameUser.findByNumber5", query = "SELECT g FROM GameUser g WHERE g.number5 = :number5"),
		@NamedQuery(name = "GameUser.findByNumber6", query = "SELECT g FROM GameUser g WHERE g.number6 = :number6"),
		@NamedQuery(name = "GameUser.findByPrice", query = "SELECT g FROM GameUser g WHERE g.price = :price") })
public class GameUser implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	private Integer id;
	
	@Basic(optional = false)
	@NotNull
	@Min(value = 1)
	@Max(value = 98)
	@Column(name = "number_1")
	private int number1;
	
	@Basic(optional = false)
	@NotNull
	@Min(value = 1)
	@Max(value = 98)
	@Column(name = "number_2")
	private int number2;
	
	@Basic(optional = false)
	@NotNull
	@Min(value = 1)
	@Max(value = 98)
	@Column(name = "number_3")
	private int number3;
	
	@Basic(optional = false)
	@NotNull
	@Min(value = 1)
	@Max(value = 98)
	@Column(name = "number_4")
	private int number4;
	
	@Basic(optional = false)
	@NotNull
	@Min(value = 1)
	@Max(value = 98)
	@Column(name = "number_5")
	private int number5;
	
	@Basic(optional = false)
	@NotNull
	@Min(value = 1)
	@Max(value = 98)
	@Column(name = "number_6")
	private int number6;
	
	@Basic(optional = false)
	@NotNull
	@Column(name = "price")
	private int price;
	
	@JoinColumn(name = "game_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Game game;
	
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private User user;
	
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "gameUser")
	private GameUserReviewed gameUserReviewed;

	public GameUser() {
	}

	public GameUser(Integer id) {
		this.id = id;
	}

	public GameUser(Integer id, int number1, int number2, int number3, int number4, int number5, int number6,
			int price) {
		this.id = id;
		this.number1 = number1;
		this.number2 = number2;
		this.number3 = number3;
		this.number4 = number4;
		this.number5 = number5;
		this.number6 = number6;
		this.price = price;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getNumber1() {
		return number1;
	}

	public void setNumber1(int number1) {
		this.number1 = number1;
	}

	public int getNumber2() {
		return number2;
	}

	public void setNumber2(int number2) {
		this.number2 = number2;
	}

	public int getNumber3() {
		return number3;
	}

	public void setNumber3(int number3) {
		this.number3 = number3;
	}

	public int getNumber4() {
		return number4;
	}

	public void setNumber4(int number4) {
		this.number4 = number4;
	}

	public int getNumber5() {
		return number5;
	}

	public void setNumber5(int number5) {
		this.number5 = number5;
	}

	public int getNumber6() {
		return number6;
	}

	public void setNumber6(int number6) {
		this.number6 = number6;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public GameUserReviewed getGameUserReviewed() {
		return gameUserReviewed;
	}

	public void setGameUserReviewed(GameUserReviewed gameUserReviewed) {
		this.gameUserReviewed = gameUserReviewed;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof GameUser)) {
			return false;
		}
		GameUser other = (GameUser) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Test.GameUser[ id=" + id + " ]";
	}

}
