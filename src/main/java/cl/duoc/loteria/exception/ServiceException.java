package cl.duoc.loteria.exception;

public class ServiceException extends Exception {
	private static final long serialVersionUID = 6769807815811590340L;

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServiceException(String message) {
		super(message);
	}

}
