package cl.duoc.loteria.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.equalTo;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import cl.duoc.loteria.config.WebConfig;
import cl.duoc.loteria.util.DateUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
public class DateUtilTest {

	@Test
	public void hourInRange_21() {
		
		Date date = DateUtil.getDate(21, 0, 0);
		
		assertThat(DateUtil.hourInRange(date), equalTo(true));
	}
	
	@Test
	public void hourInRange_0() {
		
		Date date = DateUtil.getDate(0, 0, 0);
		
		assertThat(DateUtil.hourInRange(date), equalTo(false));
	}
	
	@Test
	public void hourInRange_2059() {
		
		Date date = DateUtil.getDate(20, 59, 59);
		
		assertThat(DateUtil.hourInRange(date), equalTo(false));
	}
	
	@Test
	public void hourInRange_2359() {
		
		Date date = DateUtil.getDate(23, 59, 59);
		
		assertThat(DateUtil.hourInRange(date), equalTo(true));
	}
	
}
