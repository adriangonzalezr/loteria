package cl.duoc.loteria.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import cl.duoc.loteria.config.WebConfig;
import cl.duoc.loteria.exception.ServiceException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
public class GameReviewerServiceTest {

	@Autowired
	private GameReviewerService gameReviewerService;

	@Test
	public void contextLoadsTest() {
		assertThat(this.gameReviewerService, notNullValue());
	}
	
	@Test
	public void reviewGamesTest() throws ServiceException {
		this.gameReviewerService.reviewGames();
	}

}
