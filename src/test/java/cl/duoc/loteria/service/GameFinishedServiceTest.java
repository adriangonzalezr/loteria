package cl.duoc.loteria.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

import java.util.List;

import static org.hamcrest.Matchers.greaterThan;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import cl.duoc.loteria.config.WebConfig;
import cl.duoc.loteria.entity.GameFinished;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
public class GameFinishedServiceTest {

	@Autowired
	private GameFinishedService gameFinishedService;

	@Test
	public void contextLoadsTest() {
		assertThat(this.gameFinishedService, notNullValue());
	}

	@Test
	public void listTest() {
		List<GameFinished> gameFinisheds = this.gameFinishedService.list();

		assertThat(gameFinisheds.size(), greaterThan(0));
	}

}
