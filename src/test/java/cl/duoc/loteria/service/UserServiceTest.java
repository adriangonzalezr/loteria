package cl.duoc.loteria.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

import java.util.List;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import cl.duoc.loteria.config.WebConfig;
import cl.duoc.loteria.entity.User;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
public class UserServiceTest {

	@Autowired
	private UserService userService;

	@Test
	public void contextLoadsTest() {
		assertThat(this.userService, notNullValue());
	}

	@Test
	public void findByIdTest() {
		User user = this.userService.findById(1);

		assertThat(user.getId(), equalTo(1));
	}

}
