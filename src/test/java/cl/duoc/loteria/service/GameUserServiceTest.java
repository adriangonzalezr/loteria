package cl.duoc.loteria.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.hamcrest.Matchers.greaterThan;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import cl.duoc.loteria.config.WebConfig;
import cl.duoc.loteria.dto.GeneratedGame;
import cl.duoc.loteria.entity.Game;
import cl.duoc.loteria.entity.GameUser;
import cl.duoc.loteria.exception.ServiceException;
import cl.duoc.loteria.repository.GameRepository;
import cl.duoc.loteria.repository.UserRepository;
import cl.duoc.loteria.util.GenerateGame;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WebConfig.class)
@WebAppConfiguration
public class GameUserServiceTest {

	@Autowired
	private GameUserService gameUserService;

	@Autowired
	private GameRepository gameRepository;

	@Autowired
	private GameService gameService;

	@Autowired
	private UserRepository userRepository;

	@Test
	public void contextLoadsTest() {
		assertThat(this.gameUserService, notNullValue());
	}

	@Test
	public void findByUserId() {
		List<GameUser> gameUsers = this.gameUserService.findByUserId(1);

		assertThat(gameUsers.size(), greaterThan(0));
	}

	@Test
	public void generateGameUsers() throws ServiceException {
		Game game = this.gameService.findByGameDateOrCreate(new Date());

		for (int i = 0; i <= 1000; i++) {
			GeneratedGame generatedGame = GenerateGame.generateGame();
			
			GameUser gameUser = new GameUser();
			gameUser.setUser(userRepository.findById(1));
			gameUser.setGame(game);
			gameUser.setNumber1(generatedGame.getNumber1());
			gameUser.setNumber2(generatedGame.getNumber2());
			gameUser.setNumber3(generatedGame.getNumber3());
			gameUser.setNumber4(generatedGame.getNumber4());
			gameUser.setNumber5(generatedGame.getNumber5());
			gameUser.setNumber6(generatedGame.getNumber6());

			this.gameUserService.create(gameUser);
		}
	}
}
